import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:pluis/models/api_models/branch_office.dart';

import 'index.dart';

class BranchOfficeBloc extends Bloc<BranchOfficeEvent, BranchOfficeState> {
  static final BranchOfficeBloc _BranchOfficeBlocSingleton =
      new BranchOfficeBloc._internal();
  factory BranchOfficeBloc() {
    return _BranchOfficeBlocSingleton;
  }
  BranchOfficeBloc._internal();

  BranchOfficeState get initialState => new UnBranchOfficeState();

  @override
  Stream<BranchOfficeState> mapEventToState(
    BranchOfficeEvent event,
  ) async* {
    try {
      yield await event.applyAsync(currentState: state, bloc: this);
    } catch (_, stackTrace) {
      print('$_ $stackTrace');
      yield state;
    }
  }
}

@immutable
abstract class BranchOfficeState extends Equatable {
  const BranchOfficeState();

  /// Copy object for use in action
  BranchOfficeState copyWith();

  @override
  List<Object> get props => [];
}

/// UnInitialized
class UnBranchOfficeState extends BranchOfficeState {
  @override
  String toString() => 'UnBranchOfficeState';

  @override
  BranchOfficeState copyWith() {
    return UnBranchOfficeState();
  }

  @override
  // TODO: implement props
  List<Object> get props => null;
}

/// InInitialized
class InBranchOfficeState extends BranchOfficeState {
  List<BranchOfficeDataModel> branchOfficeDataModel;

  InBranchOfficeState({this.branchOfficeDataModel});

  @override
  String toString() => 'InBranchOfficeState';

  @override
  BranchOfficeState copyWith() {
    return InBranchOfficeState();
  }

  @override
  // TODO: implement props
  List<Object> get props => [branchOfficeDataModel];
}

/// ErrorBranchOfficeState
class ErrorBranchOfficeState extends BranchOfficeState {
  String userName;
  String avatar;
  String pass;
  String newPass;
  String error;
  int timestamp;
  ErrorBranchOfficeState(this.error,
      {this.avatar, this.userName, this.newPass, this.pass, this.timestamp});
  @override
  String toString() => 'ErrorBranchOfficeState';

  @override
  BranchOfficeState copyWith() {
    return ErrorBranchOfficeState(error);
  }

  @override
  // TODO: implement props
  List<Object> get props => [userName, avatar, error, pass, newPass, timestamp];
}

@immutable
abstract class BranchOfficeEvent {
  Future<BranchOfficeState> applyAsync(
      {BranchOfficeState currentState, BranchOfficeBloc bloc});

  final BranchOfficeRepository _branchOfficeProvider =
      new BranchOfficeRepository();
}

class LoadBranchOfficeEvent extends BranchOfficeEvent {
  @override
  String toString() => 'LoadBranchOfficeEvent';

  @override
  Future<BranchOfficeState> applyAsync(
      {BranchOfficeState currentState, BranchOfficeBloc bloc}) async {
    try {
      List<BranchOfficeDataModel> settings =
          await _branchOfficeProvider.getSettings(page: 1);
      return InBranchOfficeState(branchOfficeDataModel: settings);
    } catch (_, stackTrace) {
      print('$_ $stackTrace');
      return new ErrorBranchOfficeState(_?.toString());
    }
  }
}
