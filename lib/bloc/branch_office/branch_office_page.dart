import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';
import 'package:pluis/models/api_models/branch_office.dart';
import 'package:pluis/ui/shared/app_scaffold.dart';

import 'index.dart';

class BranchOfficePage extends StatelessWidget {
  static const String routeName = "/BranchOffice";
  BranchOfficePage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AppScaffold(
      title: Text("Sucursales"),
      leadingAction: () {
        BranchOfficeBloc().add(LoadBranchOfficeEvent());
        Navigator.of(context).pop();
        Navigator.of(context).pop();
      },
      child: BranchOfficeScreen(
        branchOfficeBloc: BranchOfficeBloc(),
      ),
    );
  }
}

class BranchOfficeScreen extends StatefulWidget {
  const BranchOfficeScreen({
    Key key,
    @required BranchOfficeBloc branchOfficeBloc,
  })  : _branchOfficeBloc = branchOfficeBloc,
        super(key: key);

  final BranchOfficeBloc _branchOfficeBloc;

  @override
  BranchOfficeScreenState createState() {
    return new BranchOfficeScreenState(_branchOfficeBloc);
  }
}

class BranchOfficeScreenState extends State<BranchOfficeScreen> {
  final BranchOfficeBloc _branchOfficeBloc;
  static const offsetVisibleThreshold = 50;
  String selectedFilter;

  BranchOfficeScreenState(this._branchOfficeBloc);

  @override
  void initState() {
    super.initState();
    _branchOfficeBloc.add(LoadBranchOfficeEvent());
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: BlocBuilder<BranchOfficeBloc, BranchOfficeState>(
        bloc: widget._branchOfficeBloc,
        builder: (
          BuildContext context,
          BranchOfficeState currentState,
        ) {
          print(currentState);
          if (currentState is UnBranchOfficeState) {
            return Container(
              color: Theme.of(context).primaryColor,
              child: Center(
                child: CircularProgressIndicator(
                  backgroundColor: Theme.of(context).accentColor,
                ),
              ),
            );
          } else if (currentState is ErrorBranchOfficeState) {
            return BranchOfficeErrorPage();
          } else if (currentState is InBranchOfficeState) {
            return BranchOfficeWidget(
              branchOfficeState: currentState,
            );
          }
          return new BranchOfficeErrorPage();
        },
      ),
    );
  }
}

class BranchOfficeErrorPage extends StatelessWidget {
  const BranchOfficeErrorPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Text("Error"),
    );
  }
}

class BranchOfficeWidget extends StatefulWidget {
  BranchOfficeWidget({Key key, this.branchOfficeState}) : super(key: key);
  final InBranchOfficeState branchOfficeState;

  @override
  _BranchOfficeWidgetState createState() => _BranchOfficeWidgetState();
}

class _BranchOfficeWidgetState extends State<BranchOfficeWidget> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 8),
      height: MediaQuery.of(context).size.height,
      child: ListView.builder(
        itemCount: widget.branchOfficeState.branchOfficeDataModel.length,
        itemBuilder: (context, index) {
          BranchOfficeDataModel item =
              widget.branchOfficeState.branchOfficeDataModel[index];
          return InkWell(
            onTap: () => onTap(item, context),
            child: ListTile(
              leading: Container(
                padding: const EdgeInsets.all(8.0),
                child: Image.network(
                  item.image,
                  height: 60,
                  fit: BoxFit.contain,
                ),
              ),
              title: Text(
                item.name,
                style: TextStyle(fontWeight: FontWeight.w700, fontSize: 14),
              ),
              subtitle: Text(item.address),
            ),
          );
        },
      ),
    );
  }

  void onTap(BranchOfficeDataModel branch, BuildContext context) {
    showModalBottomSheet(
        context: context,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(30), topRight: Radius.circular(30)),
        ),
        builder: (context) {
          return BranchOfficeSheet(
            branch: branch,
          );
        });
  }
}

class BranchOfficeSheet extends StatelessWidget {
  const BranchOfficeSheet({Key key, this.branch}) : super(key: key);
  final BranchOfficeDataModel branch;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          ListTile(
            leading: Container(
              padding: const EdgeInsets.symmetric(vertical: 2, horizontal: 4),
              child: Icon(
                Icons.info,
              ),
            ),
            title: Text(
              "Nombre",
              style: TextStyle(fontWeight: FontWeight.w700, fontSize: 14),
            ),
            subtitle: Text(
              branch.name,
              style: TextStyle(fontSize: 14),
            ),
          ),
          ListTile(
            leading: Container(
              padding: const EdgeInsets.symmetric(vertical: 2, horizontal: 4),
              child: Icon(
                Icons.location_on,
              ),
            ),
            title: Text(
              "Dirección",
              style: TextStyle(fontWeight: FontWeight.w700, fontSize: 14),
            ),
            subtitle: Text(
              branch.address,
              style: TextStyle(fontSize: 14),
            ),
          ),
          Container(
            padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 5),
            child: Text(
              "Cómo contactar:",
              style: TextStyle(fontWeight: FontWeight.w700, fontSize: 14),
            ),
          ),
          listViewContacts(context),
        ],
      ),
    );
  }

  List<Widget> getContactList() {
    List<Widget> widgets = List();
    for (var item in branch.contacts) {
      widgets.add(
        ListTile(
          leading: Container(
            padding: const EdgeInsets.all(8.0),
            child: Icon(
              Icons.info,
              color: Colors.grey,
            ),
          ),
          title: Text(
            item.type,
            style: TextStyle(fontWeight: FontWeight.w700, fontSize: 14),
          ),
          subtitle: Text(item.value),
        ),
      );
    }
    return widgets;
  }

  Widget listViewContacts(BuildContext context) {
    return Container(
        height: MediaQuery.of(context).size.height * .3,
        child: ListView.builder(
          itemCount: branch.contacts.length,
          itemBuilder: (context, index) {
            var item = branch.contacts[index];
            return ListTile(
              leading: Container(
                padding: const EdgeInsets.all(8.0),
                child: Icon(
                  Icons.info,
                  color: Colors.grey,
                ),
              ),
              title: Text(
                item.type,
                style: TextStyle(fontWeight: FontWeight.w700, fontSize: 14),
              ),
              subtitle: Text(item.value),
            );
          },
        ));
  }
}
