import 'package:pluis/models/api_models/branch_office.dart';
import 'package:pluis/models/api_models/dio_helper.dart';
import 'package:pluis/resources/sharedPreferencesController.dart';

class BranchOfficeRepository {
  Future<List<BranchOfficeDataModel>> getSettings({int page = 1}) async {
    var token = await SharedPreferencesController.getPrefAuthKey();
    var header = {"access_token": token};
    var queryParameters = {"page": page};

    var responseSettings = await DioHelper.get(
        url: "https://calzadopluis.com/admin/v1/branch-office",
        cachedPetition: true,
        headers: header,
        queryParameters: queryParameters);

    List<BranchOfficeDataModel> branchOfficeDataModel = List();
    if (responseSettings.statusCode == 200) {
      var items = responseSettings.data["items"];
      for (var item in items) {
        var json = BranchOfficeDataModel.fromJson(item);
        branchOfficeDataModel.add(json);
      }
      int currentPage = responseSettings.data["_meta"]["currentPage"];
      int totalPages = responseSettings.data["_meta"]["pageCount"];
      if (currentPage < totalPages) {
        branchOfficeDataModel.addAll(await getSettings(page: page++));
      }
    }
    return branchOfficeDataModel;
  }
}
