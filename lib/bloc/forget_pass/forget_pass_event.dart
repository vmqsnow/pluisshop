import 'dart:async';
import 'dart:developer' as developer;

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:pluis/bloc/forget_pass/index.dart';
import 'package:meta/meta.dart';
import 'package:pluis/bloc/login/index.dart';

@immutable
abstract class ForgetPassEvent {
  Stream<ForgetPassState> applyAsync(
      {ForgetPassState currentState, ForgetPassBloc bloc});
  final ForgetPassRepository _forgetPassRepository = ForgetPassRepository();
}

class UnForgetPassEvent extends ForgetPassEvent {
  @override
  Stream<ForgetPassState> applyAsync(
      {ForgetPassState currentState, ForgetPassBloc bloc}) async* {
    yield UnForgetPassState();
  }
}

class LoadForgetPassEvent extends ForgetPassEvent {
  @override
  String toString() => 'LoadForgetPassEvent';

  LoadForgetPassEvent();

  @override
  Stream<ForgetPassState> applyAsync(
      {ForgetPassState currentState, ForgetPassBloc bloc}) async* {
    try {
      yield InForgetPassState(version: 0);
    } catch (_, stackTrace) {
      developer.log('$_',
          name: 'LoadForgetPassEvent', error: _, stackTrace: stackTrace);
      yield ErrorForgetPassState(_?.toString());
    }
  }
}

class VerifyForgetPassEvent extends ForgetPassEvent {
  String email;
  @override
  String toString() => 'VerifyForgetPassEvent';

  VerifyForgetPassEvent({@required this.email});

  @override
  Stream<ForgetPassState> applyAsync(
      {ForgetPassState currentState, ForgetPassBloc bloc}) async* {
    try {
      var response = await _forgetPassRepository.sendEmail(email);
      if (response["response"]) {
        yield VerificationForgetPassState(
          version: 0,
          email: email,
        );
      } else {
        yield (currentState as InForgetPassState)
            .getStateCopy(message: response["message"]);
      }
    } catch (_, stackTrace) {
      developer.log('$_',
          name: 'LoadForgetPassEvent', error: _, stackTrace: stackTrace);
      yield ErrorForgetPassState(_?.toString());
    }
  }
}

class GoToChangePassForgetPassEvent extends ForgetPassEvent {
  String code;
  String email;
  @override
  String toString() => 'GoToChangePassForgetPassEvent';

  GoToChangePassForgetPassEvent({@required this.code, @required this.email});

  @override
  Stream<ForgetPassState> applyAsync(
      {ForgetPassState currentState, ForgetPassBloc bloc}) async* {
    try {
      var response =
          await _forgetPassRepository.sendVerificationCode(code, email);
      if (response["response"]) {
        yield ChangePassForgetPassState(
          version: 0,
          token: response["token"],
        );
      } else {
        yield (currentState as VerificationForgetPassState)
            .getStateCopy(message: response["message"]);
      }
    } catch (_, stackTrace) {
      developer.log('$_',
          name: 'GoToChangePassForgetPassEvent',
          error: _,
          stackTrace: stackTrace);
      yield ErrorForgetPassState(_?.toString());
    }
  }
}

class ChangePassForgetPassEvent extends ForgetPassEvent {
  String pass;
  String confirmationToken;
  BuildContext context;
  @override
  String toString() => 'ChangePassForgetPassEvent';

  ChangePassForgetPassEvent({
    @required this.pass,
    @required this.context,
    @required this.confirmationToken,
  });

  @override
  Stream<ForgetPassState> applyAsync(
      {ForgetPassState currentState, ForgetPassBloc bloc}) async* {
    try {
      var response =
          await _forgetPassRepository.recoveryPass(pass, confirmationToken);
      if (response["response"]) {
        await showDialog(
            barrierDismissible: false,
            context: context,
            builder: (context) {
              return AlertDialog(
                actions: <Widget>[
                  RaisedButton(
                    onPressed: () {
                      LoginBloc().add(LoadLoginEvent());
                      Navigator.of(context).pop();
                    },
                    child: Text("Aceptar"),
                  )
                ],
                title: Text(
                  "Cambio contraseña",
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: 15),
                ),
                content: Text(
                  "Se ha cambiado correctamente la contraseña",
                  style: TextStyle(
                    fontSize: 14,
                  ),
                ),
              );
            });
        yield InForgetPassState();
      } else {
        yield (currentState as ChangePassForgetPassState).getStateCopy(
          message: response["message"],
        );
      }
    } catch (_, stackTrace) {
      developer.log('$_',
          name: 'LoadForgetPassEvent', error: _, stackTrace: stackTrace);
      yield ErrorForgetPassState(_?.toString());
    }
  }
}
