import 'package:pluis/bloc/forget_pass/forget_pass_screen.dart';
import 'package:pluis/models/api_models/dio_helper.dart';
import 'package:pluis/models/api_models/login.dart';
import 'package:pluis/resources/sharedPreferencesController.dart';

class ForgetPassRepository {
  Future<Map<String, dynamic>> sendEmail(String email) async {
    try {
      Map<String, String> body = {"email": email};
      var response = await DioHelper.post(
          url: "https://calzadopluis.com/admin/v1/auth/password-recovery",
          cachedPetition: false,
          data: body);

      if (response.statusCode == 200) {
        if (response.data["status"] == "200" ||
            response.data["status"] == "201") {
          return {"response": true};
        } else {
          return {
            "response": false,
            "message": response.data["message"],
          };
        }
      }
      return {
        "response": false,
        "message": "Ha ocurrido un error",
      };
    } catch (_) {
      return {
        "response": false,
        "message": "Ha ocurrido un error",
      };
    }
  }

  Future<Map> sendVerificationCode(String code, String email) async {
    Map<String, String> body = {"confirmation_ping": code, "email": email};
    try {
      var response = await DioHelper.post(
          url:
              "https://calzadopluis.com/admin/v1/auth/password-recovery-confirmation-ping",
          cachedPetition: false,
          data: body);

      if (response.statusCode == 200) {
        if (response.data["status"] == "200") {
          return {
            "response": true,
            "token": response.data["confirmation_token"]
          };
        } else {
          return {
            "response": false,
            "message": response.data["message"],
          };
        }
      }
      return {
        "response": false,
        "message": "Ha ocurrido un error",
      };
    } catch (_) {
      return {
        "response": false,
        "message": "Ha ocurrido un error",
      };
    }
  }

  Future<Map<String, dynamic>> recoveryPass(
      String pass, String confirmationToken) async {
    Map<String, String> body = {
      "confirmation_token": confirmationToken,
      "password": pass,
      "repeat_password": pass
    };
    var response;
    try {
      response = await DioHelper.post(
          url:
              "https://calzadopluis.com/admin/v1/auth/password-recovery-receive",
          cachedPetition: false,
          data: body);
    } catch (_) {
      return {"response": false, "message": "Ha ocurrido un error"};
    }

    if (response.statusCode == 200) {
      if (response.data["status"] == "200" ||
          response.data["status"] == "201") {
        var user = User.fromJson(response.data["user"]);
        storeUser(user);
        return {"response": true};
      } else {
        return {"response": false, "message": response.data["message"]};
      }
    }
    return {"response": false, "message": "Ha ocurrido un error"};
  }

  Future<void> storeUser(User user) async {
    await SharedPreferencesController.setPrefUsername(user.username);
    await SharedPreferencesController.setPrefAddress(user.address);
    await SharedPreferencesController.setPrefEmail(user.email);
    await SharedPreferencesController.setPrefAuthKey(user.authKey);
    await SharedPreferencesController.setPrefFullname(user.fullName);
    await SharedPreferencesController.setPrefAvatarUrl(user.avatar);
    await SharedPreferencesController.setPrefCellPhone(user.phoneMobile);
  }
}
