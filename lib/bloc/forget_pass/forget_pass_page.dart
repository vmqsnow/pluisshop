import 'package:flutter/material.dart';
import 'package:pluis/bloc/forget_pass/index.dart';
import 'package:pluis/bloc/login/index.dart';
import 'package:pluis/ui/shared/app_scaffold.dart';

class ForgetPassPage extends StatefulWidget {
  static const String routeName = '/forgetPass';

  @override
  _ForgetPassPageState createState() => _ForgetPassPageState();
}

class _ForgetPassPageState extends State<ForgetPassPage> {
  final _forgetPassBloc = ForgetPassBloc();

  @override
  Widget build(BuildContext context) {
    return AppScaffold(
      title: Text(
        "Recuperación de contraseña",
        style: TextStyle(fontSize: 16),
      ),
      leadingAction: () {
        LoginBloc().add(LoadLoginEvent());
      },
      actions: <Widget>[],
      child: ForgetPassScreen(forgetPassBloc: _forgetPassBloc),
    );
  }
}
