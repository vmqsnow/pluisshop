import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

abstract class ForgetPassState extends Equatable {
  /// notify change state without deep clone state

  final List propss;
  ForgetPassState([this.propss]);

  /// Copy object for use in action
  /// if need use deep clone
  ForgetPassState getStateCopy();

  @override
  List<Object> get props => (propss ?? []);
}

/// UnInitialized
class UnForgetPassState extends ForgetPassState {
  UnForgetPassState();

  @override
  String toString() => 'UnForgetPassState';

  @override
  UnForgetPassState getStateCopy() {
    return UnForgetPassState();
  }
}

/// Initialized
class InForgetPassState extends ForgetPassState {
  String errorMessage;
  int version;
  InForgetPassState({this.version, this.errorMessage})
      : super([version, errorMessage]);

  @override
  String toString() => "InForgetPassState";

  @override
  InForgetPassState getStateCopy({String message}) {
    return InForgetPassState(version: version + 1, errorMessage: message);
  }
}

/// VerificationForgetPassState
class VerificationForgetPassState extends ForgetPassState {
  String errorMessage;
  String email;
  int version;
  VerificationForgetPassState({this.version, this.errorMessage, this.email})
      : super([version, errorMessage, email]);

  @override
  String toString() => 'VerificationForgetPassState';

  @override
  VerificationForgetPassState getStateCopy({String message}) {
    return VerificationForgetPassState(
      version: version + 1,
      errorMessage: message,
      email: email,
    );
  }
}

class ErrorForgetPassState extends ForgetPassState {
  final String errorMessage;

  ErrorForgetPassState(this.errorMessage) : super([errorMessage]);

  @override
  String toString() => 'ErrorForgetPassState';

  @override
  ErrorForgetPassState getStateCopy() {
    return ErrorForgetPassState(errorMessage);
  }
}

/// ChangePassForgetPassState
class ChangePassForgetPassState extends ForgetPassState {
  String token;
  int version;
  String erroMessage;
  ChangePassForgetPassState({
    @required this.token,
    this.version,
    this.erroMessage,
  }) : super([
          token,
          version,
          erroMessage,
        ]);

  @override
  String toString() => 'ChangePassForgetPassState';

  @override
  ChangePassForgetPassState getStateCopy({String message}) {
    return ChangePassForgetPassState(
      token: token,
      version: version + 1,
      erroMessage: message,
    );
  }
}
