import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pluis/bloc/forget_pass/index.dart';
import 'package:pluis/bloc/login/index.dart';

class ForgetPassScreen extends StatefulWidget {
  const ForgetPassScreen({
    Key key,
    @required ForgetPassBloc forgetPassBloc,
  })  : _forgetPassBloc = forgetPassBloc,
        super(key: key);

  final ForgetPassBloc _forgetPassBloc;

  @override
  ForgetPassScreenState createState() {
    return ForgetPassScreenState();
  }
}

class ForgetPassScreenState extends State<ForgetPassScreen> {
  ForgetPassScreenState();

  @override
  void initState() {
    super.initState();
    _load();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ForgetPassBloc, ForgetPassState>(
        bloc: widget._forgetPassBloc,
        builder: (
          BuildContext context,
          ForgetPassState currentState,
        ) {
          print(currentState);
          if (currentState is UnForgetPassState) {
            return Center(
              child: CircularProgressIndicator(),
            );
          } else if (currentState is ErrorForgetPassState) {
            return Center(
                child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(currentState.errorMessage ?? 'Error'),
                Padding(
                  padding: const EdgeInsets.only(top: 32.0),
                  child: RaisedButton(
                    color: Colors.blue,
                    child: Text('reload'),
                    onPressed: _load,
                  ),
                ),
              ],
            ));
          } else if (currentState is InForgetPassState) {
            return SingleChildScrollView(
                child: Container(
              height: MediaQuery.of(context).size.height,
              child: EmailSend(
                forgetPassBloc: widget._forgetPassBloc,
                errorMessage: currentState.errorMessage,
                key: ValueKey(currentState.hashCode),
              ),
            ));
          } else if (currentState is VerificationForgetPassState) {
            return SingleChildScrollView(
              child: Container(
                height: MediaQuery.of(context).size.height,
                child: VerificationCode(
                  email: currentState.email,
                  forgetPassBloc: widget._forgetPassBloc,
                  errorMessage: currentState.errorMessage,
                  key: ValueKey(currentState.hashCode),
                ),
              ),
            );
          } else if (currentState is ChangePassForgetPassState) {
            return SingleChildScrollView(
              child: Container(
                height: MediaQuery.of(context).size.height,
                child: ChangePass(
                  forgetPassBloc: widget._forgetPassBloc,
                  token: currentState.token,
                  errorMessage: currentState.erroMessage,
                  key: ValueKey(currentState.hashCode),
                ),
              ),
            );
          }
          return Center(
            child: CircularProgressIndicator(),
          );
        });
  }

  void _load([bool isError = false]) {
    widget._forgetPassBloc.add(LoadForgetPassEvent());
  }
}

class EmailSend extends StatefulWidget {
  EmailSend({
    Key key,
    @required this.forgetPassBloc,
    this.errorMessage,
  }) : super(key: key);

  ForgetPassBloc forgetPassBloc;
  String errorMessage;

  @override
  _EmailState createState() => _EmailState();
}

class _EmailState extends State<EmailSend> {
  GlobalKey<FormState> formKey = GlobalKey();
  bool autovalidate = false;
  bool loading;
  TextEditingController controller = TextEditingController();

  @override
  void initState() {
    loading = false;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: formKey,
      child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Image.asset(
              "assets/images/pluis.png",
              height: MediaQuery.of(context).size.height / 8,
            ),
            Padding(
              padding: const EdgeInsets.only(
                left: 40,
                right: 40,
                top: 20,
              ),
              child: Text(
                "Entre el correo electrónico asociado con su cuenta",
                textAlign: TextAlign.center,
                style: TextStyle(fontSize: 15),
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(
                horizontal: 40,
                vertical: 10,
              ),
              child: Text(
                "Se le enviará a su correo electrónico el código para recuperar su contraseña",
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 13,
                  color: Colors.grey,
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(
                horizontal: 40,
              ),
              child: TextFormField(
                controller: controller,
                style: TextStyle(
                  fontSize: 13,
                ),
                keyboardType: TextInputType.emailAddress,
                decoration: InputDecoration(
                  labelText: "Entre correo electrónico",
                  labelStyle: TextStyle(
                    fontSize: 13,
                  ),
                ),
                validator: validateEmail,
                autovalidate: autovalidate,
              ),
            ),
            if (widget.errorMessage != null)
              Padding(
                padding: const EdgeInsets.symmetric(
                  horizontal: 40,
                  vertical: 10,
                ),
                child: Text(
                  widget.errorMessage,
                  style: TextStyle(fontSize: 13, color: Colors.red),
                  textAlign: TextAlign.center,
                ),
              ),
            Padding(
              padding: const EdgeInsets.symmetric(
                horizontal: 40,
                vertical: 10,
              ),
              child: RaisedButton(
                onPressed: () {
                  if (!loading) {
                    if (formKey.currentState.validate()) {
                      setState(() {
                        loading = true;
                      });
                      widget.forgetPassBloc.add(
                        VerifyForgetPassEvent(
                          email: controller.text,
                        ),
                      );
                    } else {
                      setState(() {
                        autovalidate = true;
                      });
                    }
                  }
                },
                shape: StadiumBorder(),
                child: Padding(
                  padding: const EdgeInsets.symmetric(
                    horizontal: 20,
                  ),
                  child: !loading
                      ? Text(
                          "Enviar",
                          style: TextStyle(
                            color: Colors.white,
                          ),
                        )
                      : Container(
                          height: 30,
                          width: 30,
                          child: Center(
                            child: CircularProgressIndicator(
                              backgroundColor: Colors.white,
                            ),
                          ),
                        ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  String validateEmail(String value) {
    if (value.isEmpty) {
      return "Campo obligatorio";
    } else {
      Pattern pattern =
          r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
      RegExp regex = new RegExp(pattern);
      if (!regex.hasMatch(value))
        return "Debe entrar un email válido";
      else
        return null;
    }
  }
}

class VerificationCode extends StatefulWidget {
  ForgetPassBloc forgetPassBloc;
  String errorMessage;
  String email;

  VerificationCode({
    Key key,
    @required this.forgetPassBloc,
    this.errorMessage,
    this.email,
  }) : super(key: key);

  @override
  _VerificationCodeState createState() => _VerificationCodeState();
}

class _VerificationCodeState extends State<VerificationCode> {
  FocusNode fc1 = FocusNode();
  FocusNode fc2 = FocusNode();
  FocusNode fc3 = FocusNode();
  FocusNode fc4 = FocusNode();
  FocusNode fc5 = FocusNode();
  TextEditingController text1 = TextEditingController();
  TextEditingController text2 = TextEditingController();
  TextEditingController text3 = TextEditingController();
  TextEditingController text5 = TextEditingController();
  TextEditingController text4 = TextEditingController();

  GlobalKey<FormState> formKey = GlobalKey();
  bool autovalidate = false;
  bool loading;

  @override
  void initState() {
    loading = false;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: formKey,
      child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Image.asset(
              "assets/images/pluis.png",
              height: MediaQuery.of(context).size.height / 8,
            ),
            Padding(
              padding: const EdgeInsets.only(
                left: 40,
                right: 40,
                top: 20,
              ),
              child: Text(
                "Entre el código de verificación que le enviamos a su correo electrónico",
                textAlign: TextAlign.center,
                style: TextStyle(fontSize: 15),
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(
                horizontal: 40,
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Container(
                    padding: const EdgeInsets.symmetric(horizontal: 5.0),
                    width: MediaQuery.of(context).size.width * .125,
                    child: TextFormField(
                      style: TextStyle(
                        fontSize: 18,
                      ),
                      textAlign: TextAlign.center,
                      inputFormatters: [LengthLimitingTextInputFormatter(1)],
                      keyboardType: TextInputType.number,
                      controller: text1,
                      focusNode: fc1,
                      onChanged: (value) {
                        if (text1.value.text.length == 1) {
                          FocusScope.of(context).requestFocus(fc2);
                        }
                      },
                      onFieldSubmitted: (value) {
                        FocusScope.of(context).requestFocus(fc2);
                      },
                      autovalidate: autovalidate,
                      validator: (value) {
                        if (value.isEmpty) {
                          return "";
                        } else {
                          return null;
                        }
                      },
                    ),
                  ),
                  Container(
                    padding: const EdgeInsets.symmetric(horizontal: 5.0),
                    width: MediaQuery.of(context).size.width * .125,
                    child: TextFormField(
                      style: TextStyle(
                        fontSize: 18,
                      ),
                      textAlign: TextAlign.center,
                      inputFormatters: [LengthLimitingTextInputFormatter(1)],
                      keyboardType: TextInputType.number,
                      focusNode: fc2,
                      controller: text2,
                      onChanged: (value) {
                        if (text2.value.text.length == 1) {
                          FocusScope.of(context).requestFocus(fc3);
                        }
                      },
                      onFieldSubmitted: (value) {
                        FocusScope.of(context).requestFocus(fc3);
                      },
                      autovalidate: autovalidate,
                      validator: (value) {
                        if (value.isEmpty) {
                          return "";
                        } else {
                          return null;
                        }
                      },
                    ),
                  ),
                  Container(
                    padding: const EdgeInsets.symmetric(horizontal: 5.0),
                    width: MediaQuery.of(context).size.width * .125,
                    child: TextFormField(
                      style: TextStyle(
                        fontSize: 18,
                      ),
                      textAlign: TextAlign.center,
                      inputFormatters: [LengthLimitingTextInputFormatter(1)],
                      keyboardType: TextInputType.number,
                      focusNode: fc3,
                      controller: text3,
                      onChanged: (value) {
                        if (text3.value.text.length == 1) {
                          FocusScope.of(context).requestFocus(fc4);
                        }
                      },
                      onFieldSubmitted: (value) {
                        FocusScope.of(context).requestFocus(fc4);
                      },
                      autovalidate: autovalidate,
                      validator: (value) {
                        if (value.isEmpty) {
                          return "";
                        } else {
                          return null;
                        }
                      },
                    ),
                  ),
                  Container(
                    padding: const EdgeInsets.symmetric(horizontal: 5.0),
                    width: MediaQuery.of(context).size.width * .125,
                    child: TextFormField(
                      style: TextStyle(
                        fontSize: 18,
                      ),
                      textAlign: TextAlign.center,
                      inputFormatters: [LengthLimitingTextInputFormatter(1)],
                      keyboardType: TextInputType.number,
                      focusNode: fc4,
                      controller: text4,
                      onChanged: (value) {
                        if (text4.value.text.length == 1) {
                          FocusScope.of(context).requestFocus(fc5);
                        }
                      },
                      onFieldSubmitted: (value) {
                        FocusScope.of(context).requestFocus(fc5);
                      },
                      autovalidate: autovalidate,
                      validator: (value) {
                        if (value.isEmpty) {
                          return "";
                        } else {
                          return null;
                        }
                      },
                    ),
                  ),
                  Container(
                    padding: const EdgeInsets.symmetric(horizontal: 5.0),
                    width: MediaQuery.of(context).size.width * .125,
                    child: TextFormField(
                      style: TextStyle(
                        fontSize: 18,
                      ),
                      textAlign: TextAlign.center,
                      inputFormatters: [LengthLimitingTextInputFormatter(1)],
                      keyboardType: TextInputType.number,
                      focusNode: fc5,
                      controller: text5,
                      onChanged: (value) {
                        if (text5.value.text.length == 1) {
                          FocusScope.of(context).requestFocus(FocusNode());
                        }
                      },
                      onFieldSubmitted: (value) {
                        FocusScope.of(context).requestFocus(FocusNode());
                      },
                      autovalidate: autovalidate,
                      validator: (value) {
                        if (value.isEmpty) {
                          return "";
                        } else {
                          return null;
                        }
                      },
                    ),
                  ),
                ],
              ),
            ),
            if (widget.errorMessage != null)
              Padding(
                padding: const EdgeInsets.symmetric(
                  horizontal: 40,
                  vertical: 10,
                ),
                child: Text(
                  widget.errorMessage,
                  style: TextStyle(fontSize: 13, color: Colors.red),
                  textAlign: TextAlign.center,
                ),
              ),
            Padding(
              padding: const EdgeInsets.only(
                left: 40,
                right: 40,
                top: 10,
                bottom: 10,
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(
                    "Si no recibió el código! ",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: 13,
                      color: Colors.grey,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(
                      left: 10,
                    ),
                    child: InkWell(
                      onTap: () async {
                        setState(() {
                          loading = true;
                        });
                        var response = await ForgetPassRepository()
                            .sendEmail(widget.email);
                        if (response["response"]) {
                          setState(() {
                            loading = false;
                          });
                          showDialog(
                            context: context,
                            child: AlertDialog(
                              title: Text("Reenviar código"),
                              content: Text(
                                  "Se ha enviado un nuevo código a su correo electrónico"),
                              actions: <Widget>[
                                RaisedButton(
                                  child: Text("Aceptar"),
                                  onPressed: () {
                                    Navigator.of(context).pop();
                                  },
                                )
                              ],
                            ),
                          );
                        }
                      },
                      child: Text(
                        "Reenviar",
                        style: TextStyle(color: Theme.of(context).accentColor),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(
                horizontal: 40,
              ),
              child: RaisedButton(
                onPressed: () {
                  if (!loading) {
                    if (formKey.currentState.validate()) {
                      setState(() {
                        loading = true;
                      });
                      widget.forgetPassBloc.add(
                        GoToChangePassForgetPassEvent(
                          code: text1.text +
                              text2.text +
                              text3.text +
                              text4.text +
                              text5.text,
                          email: widget.email,
                        ),
                      );
                    } else {
                      setState(() {
                        autovalidate = true;
                      });
                    }
                  }
                },
                shape: StadiumBorder(),
                child: Padding(
                  padding: const EdgeInsets.symmetric(
                    horizontal: 20,
                  ),
                  child: !loading
                      ? Text(
                          "Verificar",
                          style: TextStyle(
                            color: Colors.white,
                          ),
                        )
                      : Container(
                          height: 30,
                          width: 30,
                          child: Center(
                            child: CircularProgressIndicator(
                              backgroundColor: Colors.white,
                            ),
                          ),
                        ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class ChangePass extends StatefulWidget {
  ForgetPassBloc forgetPassBloc;
  String token;
  String errorMessage;
  ChangePass({
    Key key,
    @required this.forgetPassBloc,
    @required this.token,
    this.errorMessage,
  }) : super(key: key);

  @override
  _ChangePassState createState() => _ChangePassState();
}

class _ChangePassState extends State<ChangePass> {
  FocusNode fc = FocusNode();
  TextEditingController pass = TextEditingController();
  TextEditingController repeatPass = TextEditingController();
  GlobalKey<FormState> formKey = GlobalKey();
  bool autovalidate = false;
  bool loading = false;

  @override
  Widget build(BuildContext context) {
    return Form(
      key: formKey,
      child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Image.asset(
              "assets/images/pluis.png",
              height: MediaQuery.of(context).size.height / 8,
            ),
            Padding(
              padding: const EdgeInsets.only(
                left: 40,
                right: 40,
                top: 20,
              ),
              child: Text(
                "Entre la nueva contraseña",
                textAlign: TextAlign.center,
                style: TextStyle(fontSize: 15),
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(
                horizontal: 40,
              ),
              child: TextFormField(
                style: TextStyle(
                  fontSize: 13,
                ),
                decoration: InputDecoration(
                  labelText: "Nueva contraseña",
                  labelStyle: TextStyle(
                    fontSize: 13,
                  ),
                ),
                controller: pass,
                autovalidate: autovalidate,
                validator: (value) {
                  if (value.isEmpty) {
                    return "Campo obligatorio";
                  } else {
                    return null;
                  }
                },
                onFieldSubmitted: (value) {
                  FocusScope.of(context).requestFocus(fc);
                },
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(
                horizontal: 40,
              ),
              child: TextFormField(
                style: TextStyle(
                  fontSize: 13,
                ),
                decoration: InputDecoration(
                  labelText: "Repetir contraseña",
                  labelStyle: TextStyle(
                    fontSize: 13,
                  ),
                ),
                controller: repeatPass,
                focusNode: fc,
                autovalidate: autovalidate,
                validator: (value) {
                  if (value.isEmpty) {
                    return "Campo obligatorio";
                  } else if (value != pass.text) {
                    return "Lass contraseñas deben coincidir";
                  } else {
                    return null;
                  }
                },
                onFieldSubmitted: (value) {
                  FocusScope.of(context).requestFocus(FocusNode());
                },
              ),
            ),
            if (widget.errorMessage != null)
              Padding(
                padding: const EdgeInsets.symmetric(
                  horizontal: 40,
                  vertical: 10,
                ),
                child: Text(
                  widget.errorMessage,
                  style: TextStyle(fontSize: 13, color: Colors.red),
                  textAlign: TextAlign.center,
                ),
              ),
            Padding(
              padding: const EdgeInsets.symmetric(
                horizontal: 40,
                vertical: 20,
              ),
              child: RaisedButton(
                onPressed: () async {
                  if (!loading) {
                    if (formKey.currentState.validate()) {
                      setState(() {
                        loading = true;
                      });
                      widget.forgetPassBloc.add(ChangePassForgetPassEvent(
                        context: context,
                        pass: pass.text,
                        confirmationToken: widget.token,
                      ));
                    } else {
                      setState(() {
                        autovalidate = true;
                      });
                    }
                  }
                },
                shape: StadiumBorder(),
                child: Padding(
                  padding: const EdgeInsets.symmetric(
                    horizontal: 20,
                  ),
                  child: !loading
                      ? Text(
                          "Enviar",
                          style: TextStyle(
                            color: Colors.white,
                          ),
                        )
                      : Container(
                          height: 30,
                          width: 30,
                          child: Center(
                            child: CircularProgressIndicator(
                              backgroundColor: Colors.white,
                            ),
                          ),
                        ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
