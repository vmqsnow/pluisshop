import 'dart:async';
import 'dart:developer' as developer;

import 'package:bloc/bloc.dart';
import 'package:pluis/bloc/forget_pass/index.dart';

class ForgetPassBloc extends Bloc<ForgetPassEvent, ForgetPassState> {
  // todo: check singleton for logic in project
  // use GetIt for DI in projct
  static final ForgetPassBloc _forgetPassBlocSingleton =
      ForgetPassBloc._internal();
  factory ForgetPassBloc() {
    return _forgetPassBlocSingleton;
  }
  ForgetPassBloc._internal();

  @override
  Future<void> close() async {
    // dispose objects
    await super.close();
  }

  @override
  ForgetPassState get initialState => UnForgetPassState();

  @override
  Stream<ForgetPassState> mapEventToState(
    ForgetPassEvent event,
  ) async* {
    try {
      yield* event.applyAsync(currentState: state, bloc: this);
    } catch (_, stackTrace) {
      developer.log('$_',
          name: 'ForgetPassBloc', error: _, stackTrace: stackTrace);
      yield state;
    }
  }
}
