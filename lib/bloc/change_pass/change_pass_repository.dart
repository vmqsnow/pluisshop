import 'package:pluis/models/api_models/dio_helper.dart';
import 'package:pluis/resources/sharedPreferencesController.dart';

class ChangePassRepository {
  Future<Map<String, dynamic>> changePass({String old, String newPass}) async {
    var token = await SharedPreferencesController.getPrefAuthKey();
    var header = {"access_token": token};
    var data = {
      "password": newPass,
      "repeat_password": newPass,
      "current_password": old
    };

    var response = await DioHelper.post(
        url: "https://calzadopluis.com/admin/v1/auth/change-own-password",
        cachedPetition: false,
        headers: header,
        data: data);
    if (response.statusCode == 200) {
      if (response.data["status"] == "200" ||
          response.data["status"] == "201") {
        return {"success": true, "message": response.data["message"]};
      } else {
        return {"success": false, "message": response.data["message"]};
      }
    }
    return {
      "response": false,
      "message": "Ha ocurrido un error con la conexión."
    };
  }
}
