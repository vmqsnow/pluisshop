import 'dart:async';
import 'package:flutter/material.dart';
import 'package:meta/meta.dart';
import 'package:pluis/models/api_models/category.dart';
import 'package:pluis/models/category.dart';
import 'package:pluis/models/products_cache.dart';
import 'index.dart';

@immutable
abstract class HomeEvent {
  Future<HomeState> applyAsync({HomeState currentState, HomeBloc bloc});

  final HomeRepository _homeProvider = new HomeRepository();
}

class LoadHomeEvent extends HomeEvent {
  BuildContext context;

  LoadHomeEvent({@required this.context});
  @override
  String toString() => 'LoadHomeEvent';

  @override
  Future<HomeState> applyAsync({HomeState currentState, HomeBloc bloc}) async {
    try {
      ProductCache cache = ProductCache();

      if (cache.lastUpdate == null ||
          DateTime.now().difference(cache.lastUpdate) > Duration(minutes: 5)) {
        var products = await _homeProvider.findAll();
        var sliderImages = await _homeProvider.findSliderImages(context);

        if (products.length >= 0) {
          var allCategories = await _homeProvider.findAllCategory();
          List<Category> categories = List();
          allCategories.forEach(
            (element) {
              if (categories.where((e) => e.name == element.name).isEmpty) {
                categories.add(element);
              }
            },
          );

          List<CategoryItem> categoryItems = <CategoryItem>[
            CategoryItem(name: "Todos", products: products)
          ];
          categories.forEach(
            (itemCategory) {
              var productCategorized = products.where((itemProduct) {
                String category = itemProduct.category["name"];

                String label = itemProduct.category["label"];

                if (category.contains(itemCategory.name) ||
                    label.contains(itemCategory.name)) {
                  return true;
                } else {
                  return false;
                }
              }).toList();
              if (productCategorized.length > 0) {
                categoryItems.add(CategoryItem(
                  products: productCategorized,
                  name: itemCategory.name,
                ));
              }
            },
          );

          cache.categories = categoryItems;
          cache.sliderImages = sliderImages;
          cache.lastUpdate = DateTime.now();
          return InHomeState(
            categoryItem: categoryItems,
            sliderImages: sliderImages,
          );
        } else {
          return InHomeState(
            categoryItem: cache.categories,
            sliderImages: cache.sliderImages,
          );
        }
      } else {
        return InHomeState(
          categoryItem: cache.categories,
          sliderImages: cache.sliderImages,
        );
      }
    } catch (_, stackTrace) {
      print('$_ $stackTrace');
      return new ErrorHomeState(_?.toString());
    }
  }
}

class PreLoadHomeEvent extends HomeEvent {
  @override
  String toString() => 'PreLoadHomeEvent';

  @override
  Future<HomeState> applyAsync({HomeState currentState, HomeBloc bloc}) async {
    try {
      return UnHomeState();
    } catch (_, stackTrace) {
      print('$_ $stackTrace');
      return new ErrorHomeState(_?.toString());
    }
  }
}
