import 'package:equatable/equatable.dart';
import 'package:flutter/widgets.dart';
import 'package:meta/meta.dart';
import 'package:pluis/models/category.dart';

@immutable
abstract class HomeState extends Equatable {
  HomeState();

  /// Copy object for use in action
  HomeState copyWith();
}

/// UnInitialized
class UnHomeState extends HomeState {
  @override
  String toString() => 'UnHomeState';

  @override
  HomeState copyWith() {
    return UnHomeState();
  }

  @override
  // TODO: implement props
  List<Object> get props => null;
}

/// Initialized
class InHomeState extends HomeState {
  List<CategoryItem> categoryItem;
  List<Widget> sliderImages;
  InHomeState({
    @required this.categoryItem,
    @required this.sliderImages,
  });

  @override
  String toString() => 'InHomeState';

  @override
  HomeState copyWith() {
    return InHomeState(
      categoryItem: categoryItem,
      sliderImages: sliderImages,
    );
  }

  @override
  // TODO: implement props
  List<Object> get props => null;
}

/// On Error
class ErrorHomeState extends HomeState {
  final String errorMessage;

  ErrorHomeState(this.errorMessage);

  @override
  String toString() => 'ErrorHomeState';

  @override
  HomeState copyWith() {
    return ErrorHomeState(this.errorMessage);
  }

  @override
  // TODO: implement props
  List<Object> get props => null;
}

/// No Data
class NoDataHomeState extends HomeState {
  @override
  String toString() => 'NoDataHomeState';

  @override
  HomeState copyWith() {
    return this;
  }

  @override
  // TODO: implement props
  List<Object> get props => null;
}
