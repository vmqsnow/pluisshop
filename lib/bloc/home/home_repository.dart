import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/widgets.dart';
import 'package:pluis/models/api_models/category.dart';
import 'package:pluis/models/api_models/dio_helper.dart';
import 'package:pluis/models/api_models/product.dart';

class HomeRepository {
  Future<List<Product>> findAll() async {
    int currentPage = 1;
    int totalPages = 1;
    Map responseMap = await findProductByPage(currentPage);
    totalPages = responseMap["pages"];
    List<Product> products = responseMap["products"];
    while (totalPages > currentPage) {
      currentPage++;
      responseMap = await findProductByPage(currentPage);
      products.addAll(responseMap["products"]);
    }
    return products;
  }

  Future<Map<String, dynamic>> findProductByPage(int page) async {
    Map<String, dynamic> responseMap = Map();
    List<Product> products = List<Product>();
    int totalPages = 1;

    try {
      Map<String, dynamic> headers = {"page": page};
      var response = await DioHelper.get(
        cachedPetition: true,
        url: "https://calzadopluis.com/admin/v1/product/",
        headers: headers,
      );
      if (response.statusCode == 200) {
        var answer = response.data["items"];
        for (var i = 0; i < answer.length; i++) {
          products.add(Product.fromMap(answer[i]));
        }
        totalPages = response.data["_meta"]["pageCount"];
      }
    } catch (e) {
      responseMap["products"] = List();
    }

    responseMap["products"] = products;
    responseMap["pages"] = totalPages;

    return responseMap;
  }

  Future<List<Category>> findAllCategory() async {
    try {
      List<Category> products = List();
      var response = await DioHelper.get(
        cachedPetition: true,
        url: "https://calzadopluis.com/admin/v1/category",
      );
      if (response.statusCode == 200) {
        var answer = response.data["items"];
        for (var i = 0; i < answer.length; i++) {
          products.add(Category.fromJson(answer[i]));
        }
      }
      return products;
    } catch (e) {
      return List();
    }
  }

  Future<List<Widget>> findSliderImages(BuildContext context) async {
    try {
      List<Image> images = List();

      var response = await DioHelper.get(
        cachedPetition: true,
        url: "https://calzadopluis.com/admin/v1/slider-post",
      );
      if (response.statusCode == 200) {
        var answer = response.data["items"];
        for (var i = 0; i < answer.length; i++) {
          images.add(Image(
            image: CachedNetworkImageProvider(answer[i]["responsive_image"]),
            fit: BoxFit.fill,
          ));
        }
      }

      await Future.forEach<Image>(images, (element) async {
        await precacheImage(element.image, context);
      });

      return images;
    } catch (e) {
      return List();
    }
  }
}
