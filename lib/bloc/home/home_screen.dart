import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pluis/ui/home.dart';
import 'index.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({
    Key key,
    @required HomeBloc homeBloc,
    @required BuildContext context,
  })  : _homeBloc = homeBloc,
        _context = context,
        super(key: key);

  final HomeBloc _homeBloc;
  final BuildContext _context;

  @override
  HomeScreenState createState() {
    return new HomeScreenState(_homeBloc);
  }
}

class HomeScreenState extends State<HomeScreen> {
  final HomeBloc _homeBloc;
  static const offsetVisibleThreshold = 50;
  String selectedFilter;

  HomeScreenState(this._homeBloc);

  @override
  void initState() {
    super.initState();
    _homeBloc.add(LoadHomeEvent(context: widget._context));
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      child: BlocBuilder<HomeBloc, HomeState>(
        bloc: widget._homeBloc,
        builder: (
          BuildContext context,
          HomeState currentState,
        ) {
          print(currentState);
          if (currentState is UnHomeState) {
            return HomePageUICharging();
          } else if (currentState is ErrorHomeState) {
            return Container(
                child: Text(
              "ErrorHomeState",
            ));
          } else if (currentState is NoDataHomeState) {
            return Text("NoDataHomeState");
          } else if (currentState is InHomeState) {
            return HomePageUI(
              categories: currentState.categoryItem,
              bannerImages: currentState.sliderImages,
            );
          }
          return new Container(
              child: Text(
            "Other",
          ));
        },
      ),
    );
  }
}
