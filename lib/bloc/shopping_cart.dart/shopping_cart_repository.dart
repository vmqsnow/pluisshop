import 'package:pluis/bloc/login/login_repository.dart';
import 'package:pluis/models/api_models/coupon.dart';
import 'package:pluis/models/api_models/dio_helper.dart';
import 'package:pluis/models/api_models/login.dart';
import 'package:pluis/models/api_models/payment.dart';
import 'package:pluis/models/send_models/shipping_create.dart';
import 'package:pluis/resources/sharedPreferencesController.dart';

class ShoppingCartRepository {
  Future<List<Coupon>> findAllCoupons() async {
    try {
      User user = await getUserProfile();
      List<Coupon> coupons = List();
      user.coupons.forEach((element) {
        if (coupons
            .where((coupon) => coupon.amount == element.amount)
            .isEmpty) {
          coupons.add(element);
        }
      });
      return coupons;
    } catch (e) {
      return List();
    }
  }

  Future<User> getUserProfile() async {
    var token = await SharedPreferencesController.getPrefAuthKey();
    var header = {"access_token": token};
    var response = await DioHelper.get(
        url: "https://calzadopluis.com/admin/v1/user-profile/profile",
        cachedPetition: true,
        headers: header);
    if (response.statusCode == 200) {
      if (response.data["status"] == "200" ||
          response.data["status"] == "201") {
        User user = User.fromJson(response.data["user"]);
        if (user == null) {
          return User(coupons: List());
        } else {
          return user;
        }
      }
    } else {
      return User(coupons: List());
    }
  }

  Future<List<Payment>> findAllPayments() async {
    try {
      List<Payment> products = List();
      var response = await DioHelper.get(
          cachedPetition: true,
          url: "https://calzadopluis.com/admin/v1/payment-way");
      if (response.statusCode == 200) {
        var answer = response.data["items"];
        for (var i = 0; i < answer.length; i++) {
          products.add(Payment.fromJson(answer[i]));
        }
      }
      return products;
    } catch (e) {
      return List();
    }
  }

  Future<Map<dynamic, dynamic>> processShipping(ShippingCreate shipping) async {
    try {
      var cosa = shipping.toMap();
      var response = await DioHelper.post(
          cachedPetition: true,
          url: "https://calzadopluis.com/admin/v1/shipping/create",
          data: shipping.toMap());
      if (response.statusCode == 200) {
        if (response.data["status"] == "403") {
          LoginRepository().getUserProfile();
          return await processShipping(shipping);
        }
        bool success = response.data["success"];
        if (success) {
          return {
            "response": true,
            "message":
                "Se ha realizado la compra satisfactoriamente.\nPuede realizar el seguimiento en la sección \"Seguimiento\""
          };
        } else {
          Map error = response.data["errors"];
          return {"response": false, "message": error.values.first[0]};
        }
      }
      return {"response": false, "message": "Ha ocurrido un error"};
    } catch (e) {
      return {"response": false, "message": "Ha ocurrido un error"};
    }
  }
}
