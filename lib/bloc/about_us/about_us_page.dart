import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:pluis/models/api_models/about_us.dart';
import 'package:pluis/ui/shared/app_scaffold.dart';
import 'package:pluis/ui/shared/pdf_view.dart';
import 'package:url_launcher/url_launcher.dart';

import 'index.dart';

class AboutUsPage extends StatelessWidget {
  static const String routeName = "/AboutUs";
  AboutUsPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AppScaffold(
      title: Text("Sobre nosotros"),
      leadingAction: () {
        AboutUsBloc().add(LoadAboutUsEvent());
        Navigator.of(context).pop();
        Navigator.of(context).pop();
      },
      child: AboutUsScreen(
        AboutUsBloc: AboutUsBloc(),
      ),
    );
  }
}

class AboutUsScreen extends StatefulWidget {
  const AboutUsScreen({
    Key key,
    @required AboutUsBloc AboutUsBloc,
  })  : _AboutUsBloc = AboutUsBloc,
        super(key: key);

  final AboutUsBloc _AboutUsBloc;

  @override
  AboutUsScreenState createState() {
    return new AboutUsScreenState(_AboutUsBloc);
  }
}

class AboutUsScreenState extends State<AboutUsScreen> {
  final AboutUsBloc _AboutUsBloc;
  static const offsetVisibleThreshold = 50;
  String selectedFilter;

  AboutUsScreenState(this._AboutUsBloc);

  @override
  void initState() {
    super.initState();
    _AboutUsBloc.add(LoadAboutUsEvent());
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: BlocBuilder<AboutUsBloc, AboutUsState>(
        bloc: widget._AboutUsBloc,
        builder: (
          BuildContext context,
          AboutUsState currentState,
        ) {
          print(currentState);
          if (currentState is UnAboutUsState) {
            return Container(
              color: Theme.of(context).primaryColor,
              child: Center(
                child: CircularProgressIndicator(
                  backgroundColor: Theme.of(context).accentColor,
                ),
              ),
            );
          } else if (currentState is ErrorAboutUsState) {
            return AboutUsErrorPage();
          } else if (currentState is InAboutUsState) {
            return AboutUsWidget(
              aboutUsState: currentState,
            );
          }
          return new AboutUsErrorPage();
        },
      ),
    );
  }
}

class AboutUsErrorPage extends StatelessWidget {
  const AboutUsErrorPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Text("Error"),
    );
  }
}

class AboutUsWidget extends StatefulWidget {
  AboutUsWidget({Key key, this.aboutUsState}) : super(key: key);
  final InAboutUsState aboutUsState;

  @override
  _AboutUsWidgetState createState() => _AboutUsWidgetState();
}

class _AboutUsWidgetState extends State<AboutUsWidget> {
  AboutUsDataModel dataModel;

  @override
  void initState() {
    // TODO: implement initState
    dataModel = widget.aboutUsState.aboutUsDataModel;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        height: MediaQuery.of(context).size.height,
        child: ListView(
          children: <Widget>[
            Container(
              color: Theme.of(context).primaryColor,
              padding: const EdgeInsets.symmetric(vertical: 20),
              child: Column(
                children: <Widget>[
                  Center(
                    child: Container(
                      height: 100,
                      width: 100,
                      child: CircleAvatar(
                        backgroundColor: Colors.white,
                        child: Image.network(
                          dataModel.mainLogo,
                          fit: BoxFit.contain,
                        ),
                      ),
                    ),
                  ),
                  Center(
                    child: Container(
                      padding: const EdgeInsets.only(top: 5),
                      child: Text(
                        dataModel.name,
                        style: TextStyle(
                          color: Colors.black,
                          fontWeight: FontWeight.w400,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            ListTile(
              leading: Container(
                padding: const EdgeInsets.symmetric(vertical: 4, horizontal: 8),
                child: Icon(
                  Icons.email,
                ),
              ),
              title: Text(
                "Correo electrónico",
                style: TextStyle(fontWeight: FontWeight.w700, fontSize: 14),
              ),
              subtitle: Text(
                dataModel.email,
                style: TextStyle(fontSize: 14),
              ),
            ),
            ListTile(
              leading: Container(
                padding: const EdgeInsets.symmetric(vertical: 4, horizontal: 8),
                child: Icon(
                  Icons.contact_phone,
                ),
              ),
              title: Text(
                "Teléfono",
                style: TextStyle(fontWeight: FontWeight.w700, fontSize: 14),
              ),
              subtitle: Text(
                dataModel.phone,
                style: TextStyle(fontSize: 14),
              ),
            ),
            ListTile(
              leading: Container(
                padding: const EdgeInsets.symmetric(vertical: 4, horizontal: 8),
                child: Icon(
                  Icons.location_on,
                ),
              ),
              title: Text(
                "Dirección",
                style: TextStyle(fontWeight: FontWeight.w700, fontSize: 14),
              ),
              subtitle: Text(
                dataModel.address,
                style: TextStyle(fontSize: 14),
              ),
            ),
            ListTile(
              leading: Container(
                padding: const EdgeInsets.symmetric(vertical: 4, horizontal: 8),
                child: Icon(
                  Icons.info,
                ),
              ),
              title: Text(
                "Sobre nosotros",
                style: TextStyle(fontWeight: FontWeight.w700, fontSize: 14),
              ),
              subtitle: Text(
                dataModel.aboutUs,
                style: TextStyle(fontSize: 14),
              ),
            ),
            InkWell(
              onTap: () {
                Navigator.of(context).push(MaterialPageRoute(
                  builder: (context) {
                    return CustomPdfViewer(
                      pageName: "Políticas de privacidad",
                      pdfUrl: dataModel.privacyPolicy,
                    );
                  },
                ));
              },
              child: ListTile(
                leading: Container(
                  padding:
                      const EdgeInsets.symmetric(vertical: 4, horizontal: 8),
                  child: Icon(
                    Icons.picture_as_pdf,
                  ),
                ),
                title: Text(
                  "Políticas de privacidad",
                  style: TextStyle(fontWeight: FontWeight.w700, fontSize: 14),
                ),
              ),
            ),
            InkWell(
              onTap: () {
                Navigator.of(context).push(MaterialPageRoute(
                  builder: (context) {
                    return CustomPdfViewer(
                      pageName: "Políticas de devolución",
                      pdfUrl: dataModel.devolutionPolicy,
                    );
                  },
                ));
              },
              child: ListTile(
                leading: Container(
                  padding:
                      const EdgeInsets.symmetric(vertical: 4, horizontal: 8),
                  child: Icon(
                    Icons.picture_as_pdf,
                  ),
                ),
                title: Text(
                  "Políticas de devolución",
                  style: TextStyle(fontWeight: FontWeight.w700, fontSize: 14),
                ),
              ),
            ),
            InkWell(
              onTap: () {
                Navigator.of(context).push(MaterialPageRoute(
                  builder: (context) {
                    return CustomPdfViewer(
                      pageName: "Términos y condiciones",
                      pdfUrl: dataModel.termsAndConditions,
                    );
                  },
                ));
              },
              child: ListTile(
                leading: Container(
                  padding:
                      const EdgeInsets.symmetric(vertical: 4, horizontal: 8),
                  child: Icon(
                    Icons.picture_as_pdf,
                  ),
                ),
                title: Text(
                  "Términos y condiciones",
                  style: TextStyle(fontWeight: FontWeight.w700, fontSize: 14),
                ),
              ),
            ),
            Divider(),
            Container(
              padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 5),
              child: Text(
                "Redes sociales",
                style: TextStyle(fontWeight: FontWeight.w700, fontSize: 14),
              ),
            ),
          ]..addAll(getSocialNetworkList()),
        ));
  }

  List<Widget> getSocialNetworkList() {
    List<Widget> widgets = List();
    for (var item in widget.aboutUsState.aboutUsDataModel.socialNetworks) {
      widgets.add(
        InkWell(
          onTap: () => _launchURL(item.link),
          child: ListTile(
            leading: Container(
              padding: const EdgeInsets.all(8.0),
              child: Icon(
                getIconsData(item),
                color: Colors.grey,
              ),
            ),
            title: Text(
              item.name,
              style: TextStyle(fontWeight: FontWeight.w700, fontSize: 14),
            ),
            subtitle: Text(item.link),
          ),
        ),
      );
    }
    return widgets;
  }

  IconData getIconsData(SocialNetworkModel socialMedia) {
    switch (socialMedia.name.toLowerCase()) {
      case "facebook":
        return FontAwesomeIcons.facebook;
      case "instagram":
        return FontAwesomeIcons.instagram;
      case "whatsapp":
        return FontAwesomeIcons.whatsapp;
      case "pinterest":
        return FontAwesomeIcons.pinterestP;
      case "twitter":
        return FontAwesomeIcons.twitter;
      default:
        return FontAwesomeIcons.icons;
    }
  }

  Future<void> _launchURL(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }
}
