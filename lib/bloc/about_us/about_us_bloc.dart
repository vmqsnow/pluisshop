import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:pluis/models/api_models/about_us.dart';
import 'package:pluis/resources/sharedPreferencesController.dart';

import 'index.dart';

class AboutUsBloc extends Bloc<AboutUsEvent, AboutUsState> {
  static final AboutUsBloc _AboutUsBlocSingleton = new AboutUsBloc._internal();
  factory AboutUsBloc() {
    return _AboutUsBlocSingleton;
  }
  AboutUsBloc._internal();

  AboutUsState get initialState => new UnAboutUsState();

  @override
  Stream<AboutUsState> mapEventToState(
    AboutUsEvent event,
  ) async* {
    try {
      yield await event.applyAsync(currentState: state, bloc: this);
    } catch (_, stackTrace) {
      print('$_ $stackTrace');
      yield state;
    }
  }
}

@immutable
abstract class AboutUsState extends Equatable {
  const AboutUsState();

  /// Copy object for use in action
  AboutUsState copyWith();

  @override
  List<Object> get props => [];
}

/// UnInitialized
class UnAboutUsState extends AboutUsState {
  @override
  String toString() => 'UnAboutUsState';

  @override
  AboutUsState copyWith() {
    return UnAboutUsState();
  }

  @override
  // TODO: implement props
  List<Object> get props => null;
}

/// InInitialized
class InAboutUsState extends AboutUsState {
  AboutUsDataModel aboutUsDataModel;

  InAboutUsState({this.aboutUsDataModel});

  @override
  String toString() => 'InAboutUsState';

  @override
  AboutUsState copyWith() {
    return InAboutUsState();
  }

  @override
  // TODO: implement props
  List<Object> get props => [aboutUsDataModel];
}

/// ErrorAboutUsState
class ErrorAboutUsState extends AboutUsState {
  String userName;
  String avatar;
  String pass;
  String newPass;
  String error;
  int timestamp;
  ErrorAboutUsState(this.error,
      {this.avatar, this.userName, this.newPass, this.pass, this.timestamp});
  @override
  String toString() => 'ErrorAboutUsState';

  @override
  AboutUsState copyWith() {
    return ErrorAboutUsState(error);
  }

  @override
  // TODO: implement props
  List<Object> get props => [userName, avatar, error, pass, newPass, timestamp];
}

@immutable
abstract class AboutUsEvent {
  Future<AboutUsState> applyAsync(
      {AboutUsState currentState, AboutUsBloc bloc});

  final AboutUsRepository _aboutUsProvider = new AboutUsRepository();
}

class LoadAboutUsEvent extends AboutUsEvent {
  @override
  String toString() => 'LoadAboutUsEvent';

  @override
  Future<AboutUsState> applyAsync(
      {AboutUsState currentState, AboutUsBloc bloc}) async {
    try {
      AboutUsDataModel settings = await _aboutUsProvider.getSettings();
      return InAboutUsState(aboutUsDataModel: settings);
    } catch (_, stackTrace) {
      print('$_ $stackTrace');
      return new ErrorAboutUsState(_?.toString());
    }
  }
}
