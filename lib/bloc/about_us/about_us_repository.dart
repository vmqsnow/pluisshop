import 'package:pluis/models/api_models/about_us.dart';
import 'package:pluis/models/api_models/dio_helper.dart';
import 'package:pluis/resources/sharedPreferencesController.dart';

class AboutUsRepository {
  Future<AboutUsDataModel> getSettings({String old, String newPass}) async {
    var token = await SharedPreferencesController.getPrefAuthKey();
    var header = {"access_token": token};

    var responseSettings = await DioHelper.get(
      url: "https://calzadopluis.com/admin/v1/setting",
      cachedPetition: true,
      headers: header,
    );
    var responseSocial = await DioHelper.get(
      url: "https://calzadopluis.com/admin/v1/social-network",
      cachedPetition: true,
      headers: header,
    );

    AboutUsDataModel aboutUsDataModel;
    if (responseSettings.statusCode == 200) {
      var settings = responseSettings.data["settings"];
      aboutUsDataModel = AboutUsDataModel.fromJson(settings);
      if (responseSocial.statusCode == 200) {
        for (var item in responseSocial.data["items"]) {
          var json = SocialNetworkModel.formJson(item);
          aboutUsDataModel.socialNetworks.add(json);
        }
      }
    }
    return aboutUsDataModel;
  }
}
