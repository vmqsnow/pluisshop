import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pluis/ui/shared/app_scaffold.dart';

import 'index.dart';

class ProfilePage extends StatefulWidget {
  static const String routeName = "/profile";
  ProfilePage({Key key}) : super(key: key);

  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  ProfileBloc _profileBloc = ProfileBloc();

  TextEditingController nameController = TextEditingController();
  TextEditingController lastNameController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController phoneController = TextEditingController();
  TextEditingController addressController = TextEditingController();

  @override
  void initState() {
    _profileBloc.add(LoadProfileEvent());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ProfileBloc, ProfileState>(
      bloc: _profileBloc,
      builder: (
        BuildContext context,
        ProfileState currentState,
      ) {
        return AppScaffold(
          title: currentState is EditProfileState
              ? Text("Edición")
              : Text("Perfil"),
          leadingAction: () {
            Navigator.of(context).pop();
          },
          actions: <Widget>[
            if (currentState is InProfileState)
              InkWell(
                onTap: () {
                  ProfileBloc().add(GoToEditProfileEvent());
                },
                child: Center(
                  child: Container(
                    padding: const EdgeInsets.only(
                      right: 5,
                    ),
                    child: Text(
                      "Editar",
                      textAlign: TextAlign.center,
                    ),
                  ),
                ),
              ),
            if (currentState is EditProfileState)
              InkWell(
                onTap: () {
                  if (currentState.formkey.currentState.validate()) {
                    _profileBloc.add(
                      EditProfileEvent(
                        address: addressController.text,
                        email: emailController.text,
                        name: nameController.text,
                        lastName: lastNameController.text,
                        phone: phoneController.text,
                        context: context,
                      ),
                    );
                  }
                },
                child: Center(
                  child: Container(
                    padding: const EdgeInsets.only(
                      right: 5,
                    ),
                    child: Text(
                      "Aceptar",
                      textAlign: TextAlign.center,
                    ),
                  ),
                ),
              ),
          ],
          child: getContent(context, currentState),
        );
      },
    );
  }

  Widget getContent(BuildContext context, ProfileState currentState) {
    print(currentState);
    if (currentState is UnProfileState) {
      return Container(
        color: Theme.of(context).primaryColor,
        child: Center(
          child: CircularProgressIndicator(
            backgroundColor: Colors.transparent,
          ),
        ),
      );
    } else if (currentState is ErrorProfileState) {
      return Container(
        color: Theme.of(context).primaryColor,
        child: Center(
          child: Text(currentState.errorMessage),
        ),
      );
    } else if (currentState is NoDataProfileState) {
      return Text("NoDataProfileState");
    } else if (currentState is InProfileState) {
      return ProfileWidget(
        profileState: currentState,
      );
    } else if (currentState is EditProfileState) {
      nameController.text = currentState.firstName;
      lastNameController.text = currentState.lastName;
      emailController.text = currentState.email;
      phoneController.text = currentState.phone;
      addressController.text = currentState.address;

      return EditProfile(
        formKey: currentState.formkey,
        nameController: nameController,
        lastNameController: lastNameController,
        emailController: emailController,
        phoneController: phoneController,
        addresController: addressController,
      );
    }
    return new Container(
        child: Text(
      "Other",
    ));
  }
}
