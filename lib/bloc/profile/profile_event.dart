import 'dart:async';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:meta/meta.dart';
import 'index.dart';

@immutable
abstract class ProfileEvent {
  Future<ProfileState> applyAsync(
      {ProfileState currentState, ProfileBloc bloc});

  final ProfileRepository _profileProvider = new ProfileRepository();
}

class LoadProfileEvent extends ProfileEvent {
  @override
  String toString() => 'LoadProfileEvent';

  @override
  Future<ProfileState> applyAsync(
      {ProfileState currentState, ProfileBloc bloc}) async {
    try {
      var user = await _profileProvider.getUserProfile();

      return InProfileState(
          address: user.address,
          avatar: user.avatar,
          username: user.username,
          email: user.email,
          phone: user.phoneMobile,
          fullname: user.fullName,
          firstName: user.firstName,
          lastName: user.lastName,
          coupons: user.coupons);
    } catch (_, stackTrace) {
      print('$_ $stackTrace');
      return new ErrorProfileState(_?.toString());
    }
  }
}

class GoToEditProfileEvent extends ProfileEvent {
  @override
  String toString() => 'LoadProfileEvent';

  @override
  Future<ProfileState> applyAsync(
      {ProfileState currentState, ProfileBloc bloc}) async {
    try {
      return EditProfileState(
        address: (currentState as InProfileState).address,
        avatar: (currentState as InProfileState).avatar,
        username: (currentState as InProfileState).username,
        email: (currentState as InProfileState).email,
        phone: (currentState as InProfileState).phone,
        firstName: (currentState as InProfileState).firstName,
        lastName: (currentState as InProfileState).lastName,
        formkey: GlobalKey(),
      );
    } catch (_, stackTrace) {
      print('$_ $stackTrace');
      return new ErrorProfileState(_?.toString());
    }
  }
}

class EditProfileEvent extends ProfileEvent {
  String name, lastName, email, phone, address;
  BuildContext context;
  EditProfileEvent({
    this.name,
    this.lastName,
    this.address,
    this.phone,
    this.email,
    this.context,
  });
  @override
  String toString() => 'LoadProfileEvent';

  @override
  Future<ProfileState> applyAsync(
      {ProfileState currentState, ProfileBloc bloc}) async {
    try {
      Map<String, dynamic> data = {
        "first_name": name,
        "last_name": lastName,
        "address": address,
        "email": email,
        "phone_mobile": phone,
      };
      var user = await _profileProvider.editUserProfile(data);

      await showDialog(
          context: context,
          barrierDismissible: false,
          builder: (context) {
            return AlertDialog(
              title: Text("Edición de datos"),
              content: Text(user != null
                  ? "Se han modificado sus datos correctamente"
                  : "No se pudieron actualizar los datos"),
              actions: <Widget>[
                FlatButton(
                  onPressed: () {
                    Navigator.of(context).pop(true);
                  },
                  child: Text("Aceptar"),
                )
              ],
            );
          });

      if (user != null) {
        return InProfileState(
            address: user.address,
            avatar: user.avatar,
            username: user.username,
            email: user.email,
            phone: user.phoneMobile,
            fullname: user.fullName,
            coupons: user.coupons);
      } else {
        return currentState;
      }
    } catch (_, stackTrace) {
      print('$_ $stackTrace');
      return new ErrorProfileState(_?.toString());
    }
  }
}
