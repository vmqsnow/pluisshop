import 'package:flutter/material.dart';
import 'package:pluis/models/api_models/coupon.dart';
import 'index.dart';

class ProfileWidget extends StatefulWidget {
  ProfileWidget({Key key, this.profileState}) : super(key: key);
  final InProfileState profileState;

  @override
  _ProfileWidgetState createState() => _ProfileWidgetState();
}

class _ProfileWidgetState extends State<ProfileWidget> {
  @override
  Widget build(BuildContext context) {
    return Container(
        height: MediaQuery.of(context).size.height,
        child: ListView(
          children: <Widget>[
            Container(
              color: Theme.of(context).primaryColor,
              padding: const EdgeInsets.symmetric(vertical: 20),
              child: Column(
                children: <Widget>[
                  Center(
                    child: Container(
                      height: 100,
                      width: 100,
                      child: CircleAvatar(
                        backgroundImage:
                            NetworkImage(widget.profileState.avatar),
                      ),
                    ),
                  ),
                  Center(
                    child: Container(
                      padding: const EdgeInsets.only(top: 5),
                      child: Text(
                        widget.profileState.fullname,
                        style: TextStyle(
                          color: Colors.black,
                          fontWeight: FontWeight.w400,
                        ),
                      ),
                    ),
                  ),
                  Center(
                    child: Text(
                      "@${widget.profileState.username}",
                      style: TextStyle(fontWeight: FontWeight.w400),
                    ),
                  ),
                ],
              ),
            ),
            ListTile(
              leading: Container(
                padding: const EdgeInsets.all(8.0),
                child: Icon(
                  Icons.account_circle,
                ),
              ),
              title: Text(
                "Nombre",
                style: TextStyle(fontWeight: FontWeight.w700, fontSize: 14),
              ),
              subtitle: Text(
                widget.profileState.fullname,
                style: TextStyle(fontSize: 14),
              ),
            ),
            ListTile(
              leading: Container(
                padding: const EdgeInsets.all(8.0),
                child: Icon(
                  Icons.email,
                ),
              ),
              title: Text(
                "Correo electrónico",
                style: TextStyle(fontWeight: FontWeight.w700, fontSize: 14),
              ),
              subtitle: Text(
                widget.profileState.email,
                style: TextStyle(fontSize: 14),
              ),
            ),
            ListTile(
              leading: Container(
                padding: const EdgeInsets.all(8.0),
                child: Icon(
                  Icons.contact_phone,
                ),
              ),
              title: Text(
                "Teléfono",
                style: TextStyle(fontWeight: FontWeight.w700, fontSize: 14),
              ),
              subtitle: Text(
                widget.profileState.phone,
                style: TextStyle(fontSize: 14),
              ),
            ),
            ListTile(
              leading: Container(
                padding: const EdgeInsets.all(8.0),
                child: Icon(
                  Icons.location_on,
                ),
              ),
              title: Text(
                "Dirección",
                style: TextStyle(fontWeight: FontWeight.w700, fontSize: 14),
              ),
              subtitle: Text(
                widget.profileState.address,
                style: TextStyle(fontSize: 14),
              ),
            ),
            Divider(),
            Container(
              padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 5),
              child: Text("Cupones disponibles",
                  style: TextStyle(fontWeight: FontWeight.w700, fontSize: 14)),
            ),
            CouponsList(coupons: widget.profileState.coupons)
          ],
        ));
  }
}

class CouponsList extends StatelessWidget {
  CouponsList({Key key, this.coupons}) : super(key: key);
  final List<Coupon> coupons;

  @override
  Widget build(BuildContext context) {
    return CustomScrollView(shrinkWrap: true, slivers: <Widget>[
      SliverGrid(
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 2, childAspectRatio: 2.5),
        delegate: SliverChildBuilderDelegate(
          (context, index) {
            Coupon item = coupons[index];
            return ListTile(
              leading: Container(
                padding: const EdgeInsets.all(8.0),
                child: Icon(
                  Icons.local_activity,
                  color: item.expireUsing ? Colors.green : Colors.grey,
                ),
              ),
              title: Text(
                "Descuento",
                style: TextStyle(fontWeight: FontWeight.w700, fontSize: 14),
              ),
              subtitle: Text("${item.amount.toString()}%"),
            );
          },
          childCount: coupons.length,
        ),
      )
    ]);
  }
}

class EditProfile extends StatelessWidget {
  EditProfile({
    Key key,
    this.nameController,
    this.lastNameController,
    this.emailController,
    this.phoneController,
    this.addresController,
    this.formKey,
  }) : super(key: key);
  GlobalKey<FormState> formKey;
  TextEditingController nameController;
  TextEditingController lastNameController;
  TextEditingController emailController;
  TextEditingController phoneController;
  TextEditingController addresController;

  @override
  Widget build(BuildContext context) {
    return Form(
      key: formKey,
      child: Container(
          height: MediaQuery.of(context).size.height,
          child: ListView(
            children: <Widget>[
              Container(
                padding: const EdgeInsets.symmetric(
                  horizontal: 25,
                  vertical: 8,
                ),
                child: TextFormField(
                  controller: nameController,
                  style: TextStyle(fontSize: 14),
                  decoration: InputDecoration(
                    icon: Icon(
                      Icons.account_circle,
                    ),
                    labelText: "Nombre",
                    labelStyle:
                        TextStyle(fontWeight: FontWeight.w700, fontSize: 14),
                    border: InputBorder.none,
                    floatingLabelBehavior: FloatingLabelBehavior.auto,
                  ),
                  keyboardType: TextInputType.text,
                  validator: (value) {
                    if (value.isEmpty) {
                      return "Campo obligatorio";
                    }
                    return null;
                  },
                  onSaved: (value) {},
                ),
              ),
              Container(
                padding: const EdgeInsets.symmetric(
                  horizontal: 25,
                  vertical: 8,
                ),
                child: TextFormField(
                  controller: lastNameController,
                  style: TextStyle(fontSize: 14),
                  decoration: InputDecoration(
                    icon: Icon(
                      Icons.account_circle,
                    ),
                    labelText: "Apellidos",
                    labelStyle:
                        TextStyle(fontWeight: FontWeight.w700, fontSize: 14),
                    border: InputBorder.none,
                    floatingLabelBehavior: FloatingLabelBehavior.auto,
                  ),
                  keyboardType: TextInputType.text,
                  validator: (value) {
                    if (value.isEmpty) {
                      return "Campo obligatorio";
                    }
                    return null;
                  },
                  onSaved: (value) {},
                ),
              ),
              Container(
                padding: const EdgeInsets.symmetric(
                  horizontal: 25,
                  vertical: 8,
                ),
                child: TextFormField(
                  controller: emailController,
                  style: TextStyle(fontSize: 14),
                  decoration: InputDecoration(
                    icon: Icon(
                      Icons.email,
                    ),
                    labelText: "Correo electrónico",
                    labelStyle:
                        TextStyle(fontWeight: FontWeight.w700, fontSize: 14),
                    border: InputBorder.none,
                    floatingLabelBehavior: FloatingLabelBehavior.auto,
                  ),
                  keyboardType: TextInputType.emailAddress,
                  validator: (value) {
                    if (value.isEmpty) {
                      return "Campo obligatorio";
                    }
                    return null;
                  },
                  onSaved: (value) {},
                ),
              ),
              Container(
                padding: const EdgeInsets.symmetric(
                  horizontal: 25,
                  vertical: 8,
                ),
                child: TextFormField(
                  controller: phoneController,
                  style: TextStyle(fontSize: 14),
                  decoration: InputDecoration(
                    icon: Icon(
                      Icons.contact_phone,
                    ),
                    labelText: "Teléfono",
                    labelStyle:
                        TextStyle(fontWeight: FontWeight.w700, fontSize: 14),
                    border: InputBorder.none,
                    floatingLabelBehavior: FloatingLabelBehavior.auto,
                  ),
                  keyboardType: TextInputType.phone,
                  validator: (value) {
                    if (value.isEmpty) {
                      return "Campo obligatorio";
                    }
                    return null;
                  },
                  onSaved: (value) {},
                ),
              ),
              Container(
                padding: const EdgeInsets.symmetric(
                  horizontal: 25,
                  vertical: 8,
                ),
                child: TextFormField(
                  controller: addresController,
                  style: TextStyle(fontSize: 14),
                  decoration: InputDecoration(
                    icon: Icon(
                      Icons.location_on,
                    ),
                    labelText: "Dirección",
                    labelStyle:
                        TextStyle(fontWeight: FontWeight.w700, fontSize: 14),
                    border: InputBorder.none,
                    floatingLabelBehavior: FloatingLabelBehavior.auto,
                  ),
                  keyboardType: TextInputType.text,
                  validator: (value) {
                    if (value.isEmpty) {
                      return "Campo obligatorio";
                    }
                    return null;
                  },
                  onSaved: (value) {},
                ),
              ),
            ],
          )),
    );
  }
}
