import 'package:flutter/widgets.dart';

import 'category.dart';

class ProductCache {
  static final ProductCache _singleton = ProductCache._internal();
  List<CategoryItem> categories;
  List<Widget> sliderImages;
  DateTime lastUpdate;

  factory ProductCache() {
    return _singleton;
  }

  ProductCache._internal();
}
