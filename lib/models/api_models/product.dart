import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:pluis/models/api_models/related_product.dart';
import 'package:pluis/models/api_models/sku.dart';

class ProductSetting {
  String name;
  String url;
  bool limitedEdition;
  bool inProduction;
  bool highlight;
  double priceCup;
  double priceUsd;
  String abstract;
  Map category;
  String image;
  List<dynamic> gallery;
  List<Sku> sku;
  List<RelatedProduct> relatedProducts;
  String description;
  bool active;

  ProductSetting(
      {this.name,
      this.url,
      this.limitedEdition,
      this.inProduction,
      this.highlight,
      this.priceCup,
      this.priceUsd,
      this.abstract,
      this.category,
      this.image,
      this.gallery,
      this.sku,
      this.relatedProducts,
      this.description,
      this.active});

  ProductSetting.fromJson(Map json)
      : name =
            json["name"] == false || json["name"] == null ? "" : json["name"],
        url = json["url"] == false || json["url"] == null ? "" : json["url"],
        limitedEdition = json["limited_edition"],
        inProduction = json["in_production"],
        highlight = json["highlight"],
        priceCup = json["price_cup"].toDouble(),
        priceUsd = json["price_usd"].toDouble(),
        abstract = json["abstract"] == false || json["abstract"] == null
            ? ""
            : json["abstract"],
        category = json["category"],
        image = json["image"] == false || json["image"] == null
            ? ""
            : json["image"],
        gallery = json["gallery"],
        sku = (json["sku"].map<Sku>((item) => Sku.fromJson(jsonDecode(item))))
            .toList(),
        relatedProducts = json["related_products"] != null
            ? (json["related_products"].map<RelatedProduct>(
                (item) => RelatedProduct.fromJson(jsonDecode(item)))).toList()
            : null,
        description =
            json["description"] == false || json["description"] == null
                ? ""
                : json["description"],
        active = json["active"];

  ProductSetting.fromMap(Map json)
      : name =
            json["name"] == false || json["name"] == null ? "" : json["name"],
        url = json["url"] == false || json["url"] == null ? "" : json["url"],
        limitedEdition = json["limited_edition"],
        inProduction = json["in_production"],
        highlight = json["highlight"],
        priceCup = json["price_cup"].toDouble(),
        priceUsd = json["price_usd"].toDouble(),
        abstract = json["abstract"] == false || json["abstract"] == null
            ? ""
            : json["abstract"],
        category = json["category"],
        image = json["image"] ?? "",
        gallery = json["gallery"],
        sku = (json["sku"].map<Sku>((item) => Sku.fromMap(item))).toList(),
        relatedProducts = json["related_products"] != null
            ? (json["related_products"].map<RelatedProduct>(
                (item) => RelatedProduct.fromJson(item))).toList()
            : null,
        description =
            json["description"] == false || json["description"] == null
                ? ""
                : json["description"],
        active = json["active"];

  Map<String, dynamic> toMap() {
    return {
      "name": this.name,
      "url": this.url,
      "limited_edition": this.limitedEdition,
      "in_production": this.inProduction,
      "highlight": this.highlight,
      "price_cup": this.priceCup,
      "price_usd": this.priceUsd,
      "abstract": this.abstract,
      "category": this.category,
      "image": this.image,
      "gallery": this.gallery,
      "sku": this.sku,
      "related_products": this.relatedProducts,
      "description": this.description,
      "active": this.active,
    };
  }

  String toJson() {
    return jsonEncode(ProductSetting(
            name: name,
            url: url,
            limitedEdition: limitedEdition,
            inProduction: inProduction,
            highlight: highlight,
            priceCup: priceCup,
            priceUsd: priceUsd,
            abstract: abstract,
            category: category,
            image: image,
            gallery: gallery,
            sku: sku,
            relatedProducts: relatedProducts,
            description: description,
            active: active)
        .toMap());
  }

  // String getSizes() {
  //   var result = "";
  //   sku.forEach((i) {
  //     result += "${i.talla} ";
  //   });
  //   return result.trim();
  // }

  // List<Widget> getColors() {
  //   var colors = <Widget>[];
  //   sku.forEach((i) {
  //     colors.addAll(i.options.map<Widget>((f) => Container(
  //           margin: const EdgeInsets.symmetric(horizontal: 1),
  //           height: 13,
  //           width: 13,
  //           color: getColorFromHex(f).withOpacity(1),
  //         )));
  //   });
  //   return colors;
  // }

  // Color getColorFromHex(SkuOption option) {
  //   return Color(int.parse(option.color.replaceFirst("#", ""), radix: 16));
  // }

  String getId() {
    return url.split('=')[1];
  }
}

class Product {
  int id;
  Map category;
  String name;
  String url;
  String image;
  double priceCup;
  double priceUsd;
  List<dynamic> gallery;
  List<Sku> sku;
  List<RelatedProduct> relatedProducts;
  List<RelatedProduct> complementaryProducts;

  Product({
    this.id,
    this.category,
    this.name,
    this.url,
    this.image,
    this.priceCup,
    this.priceUsd,
    this.gallery,
    this.sku,
    this.relatedProducts,
    this.complementaryProducts,
  });

  Product.fromMap(Map json)
      : id = json["id"],
        category = json["category"],
        name =
            json["name"] == false || json["name"] == null ? "" : json["name"],
        url = json["url"],
        image = json["image"] == false || json["image"] == null
            ? ""
            : json["image"],
        priceCup = json["price_cup"].toDouble(),
        priceUsd = json["price_usd"].toDouble(),
        gallery = json["gallery"],
        sku = (json["sku"].map<Sku>((item) => Sku.fromMap(item))).toList(),
        relatedProducts = json["related_products"] != null
            ? (json["related_products"].map<RelatedProduct>(
                (item) => RelatedProduct.fromJson(item))).toList()
            : null,
        complementaryProducts = json["complementary_products"] != null
            ? (json["complementary_products"].map<RelatedProduct>(
                (item) => RelatedProduct.fromJson(item))).toList()
            : null;

  Product.fromJson(Map json)
      : id = json["id"],
        category = json["category"],
        name =
            json["name"] == false || json["name"] == null ? "" : json["name"],
        url = json["url"],
        image = json["image"] == false || json["image"] == null
            ? ""
            : json["image"],
        priceCup = json["price_cup"].toDouble(),
        priceUsd = json["price_usd"].toDouble(),
        gallery = json["gallery"],
        sku = (json["sku"].map<Sku>((item) => Sku.fromJson(jsonDecode(item))))
            .toList(),
        relatedProducts = json["related_products"] != null
            ? (json["related_products"].map<RelatedProduct>(
                (item) => RelatedProduct.fromJson(jsonDecode(item)))).toList()
            : null,
        complementaryProducts = json["complementary_products"] != null
            ? (json["complementary_products"].map<RelatedProduct>(
                (item) => RelatedProduct.fromJson(jsonDecode(item)))).toList()
            : null;

  Map<String, dynamic> toMap() {
    return {
      "id": this.id,
      "category": this.category,
      "name": this.name,
      "url": this.url,
      "image": this.image,
      "price_cup": this.priceCup,
      "price_usd": this.priceUsd,
      "gallery": this.gallery,
      "sku": this.sku,
      "related_products": this.relatedProducts,
      "complementary_products": this.complementaryProducts,
    };
  }

  String toJson() {
    return jsonEncode(Product(
      id: id,
      category: category,
      name: name,
      url: url,
      image: image,
      priceCup: priceCup,
      priceUsd: priceUsd,
      gallery: gallery,
      sku: sku,
      relatedProducts: relatedProducts,
      complementaryProducts: complementaryProducts,
    ).toMap());
  }

  String getId() {
    return url.split('=')[1];
  }

  String getSizes() {
    var result = "";
    sku.forEach((i) {
      result += "${i.talla} ";
    });
    return result.trim();
  }

  List<Widget> getColors() {
    var colors = <Widget>[];
    sku.forEach((i) {
      colors.addAll(i.options.map<Widget>((f) => Container(
            margin: const EdgeInsets.symmetric(horizontal: 1),
            height: 13,
            width: 13,
            color: getColorFromHex(f).withOpacity(1),
          )));
    });
    return colors;
  }

  Color getColorFromHex(SkuOption option) {
    return Color(int.parse(option.color.replaceFirst("#", ""), radix: 16));
  }
}
