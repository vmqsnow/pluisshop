import 'dart:convert';

class RelatedProduct {
  String name;
  String url;
  double priceCup;
  double priceUsd;
  String abstract;
  Map category;
  String image;

  RelatedProduct({
    this.name,
    this.url,
    this.priceCup,
    this.priceUsd,
    this.abstract,
    this.category,
    this.image,
  });

  RelatedProduct.fromJson(Map json)
      : name = json["name"],
        url = json["url"],
        priceCup = json["price_cup"].toDouble(),
        priceUsd = json["price_usd"].toDouble(),
        abstract = json["abstract"],
        category = json["category"],
        image = json["image"];

  Map<String, dynamic> toMap() {
    return {
      "name": this.name,
      "url": this.url,
      "price_cup": this.priceCup,
      "price_usd": this.priceUsd,
      "abstract": this.abstract,
      "category": this.category,
      "image": this.image,
    };
  }

  String toJson() {
    return jsonEncode(RelatedProduct(
      name: name,
      url: url,
      priceCup: priceCup,
      priceUsd: priceUsd,
      abstract: abstract,
      category: category,
      image: image,
    ).toMap());
  }
}
