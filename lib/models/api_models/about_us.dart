import 'package:html/parser.dart';

class AboutUsDataModel {
  String name;
  String email;
  String phone;
  String aboutUs;
  String address;
  String privacyPolicy;
  String devolutionPolicy;
  String termsAndConditions;
  String mainLogo;
  String headerLogo;
  String miniLogo;
  List<SocialNetworkModel> socialNetworks;

  AboutUsDataModel({
    this.name,
    this.email,
    this.phone,
    this.aboutUs,
    this.address,
    this.privacyPolicy,
    this.devolutionPolicy,
    this.termsAndConditions,
    this.mainLogo,
    this.headerLogo,
    this.miniLogo,
  }) {
    socialNetworks = List();
  }

  AboutUsDataModel.fromJson(Map json)
      : name = json["name"],
        email = json["email"],
        phone = json["phone"],
        aboutUs = parseHtmlText(json["about_us"]),
        address = json["address"],
        privacyPolicy = json["privacy_policy"],
        devolutionPolicy = json["devolution_policy"],
        termsAndConditions = json["terms_and_conditions"],
        mainLogo = json["main_logo"],
        headerLogo = json["header_logo"],
        miniLogo = json["mini_logo"],
        socialNetworks = List();

  static String parseHtmlText(String htmlText) {
    var document = parse(htmlText);
    String parsedString = parse(document.body.text).documentElement.text;
    return parsedString;
  }
}

class SocialNetworkModel {
  String name;
  String link;

  SocialNetworkModel({
    this.name,
    this.link,
  });

  SocialNetworkModel.formJson(Map json)
      : name = json["name"],
        link = json["link"];
}
