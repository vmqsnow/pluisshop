class BranchOfficeDataModel {
  String name;
  String address;
  String image;
  List<BranchOfficeContacts> contacts;

  BranchOfficeDataModel({this.name, this.address, this.image});

  BranchOfficeDataModel.fromJson(Map json)
      : name = json["name"],
        address = json["address"],
        image = json["image"],
        contacts = (json["contacts"].map<BranchOfficeContacts>(
            (item) => BranchOfficeContacts.fromJson(item))).toList();
}

class BranchOfficeContacts {
  String type;
  String value;

  BranchOfficeContacts({this.type, this.value});

  BranchOfficeContacts.fromJson(Map json)
      : type = json["type"],
        value = json["value"];
}
