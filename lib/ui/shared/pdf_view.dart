import 'dart:async';
import 'dart:io';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_pdfview/flutter_pdfview.dart';
import 'package:path_provider/path_provider.dart';

import 'package:flutter/material.dart';
import 'package:pluis/bloc/branch_office/index.dart';
import 'package:pluis/ui/shared/app_scaffold.dart';

class CustomPdfViewer extends StatelessWidget {
  const CustomPdfViewer({
    Key key,
    @required this.pdfUrl,
    @required this.pageName,
  }) : super(key: key);
  final String pdfUrl;
  final String pageName;

  Future<File> loadPdf() async {
    Completer<File> completer = Completer();
    try {
      final fileName = pdfUrl.substring(pdfUrl.lastIndexOf("/") + 1);
      var request = await HttpClient().getUrl(Uri.parse(pdfUrl));
      var response = await request.close();
      var bytes = await consolidateHttpClientResponseBytes(response);
      var dir = await getApplicationDocumentsDirectory();
      File file = File("${dir.path}/$fileName");
      await file.writeAsBytes(bytes);
      completer.complete(file);
    } catch (_) {
      throw Exception("Error cargando documento");
    }
    return completer.future;
  }

  @override
  Widget build(BuildContext context) {
    return AppScaffold(
        title: AutoSizeText(
          pageName,
          maxLines: 1,
        ),
        leadingAction: () {
          Navigator.of(context).pop();
        },
        child: FutureBuilder(
          future: loadPdf(),
          builder: (context, AsyncSnapshot<File> snapshot) {
            if (snapshot.connectionState == ConnectionState.done &&
                snapshot.data != null) {
              return PdfScreen(snapshot.data.path);
            } else {
              return Container(
                color: Theme.of(context).primaryColor,
                child: Center(
                  child: CircularProgressIndicator(
                    backgroundColor: Theme.of(context).accentColor,
                  ),
                ),
              );
            }
          },
        ));
  }
}

class PdfScreen extends StatefulWidget {
  const PdfScreen(this.path, {Key key}) : super(key: key);
  final String path;

  @override
  _PdfScreenState createState() => _PdfScreenState();
}

class _PdfScreenState extends State<PdfScreen> {
  Completer<PDFViewController> _completerController =
      Completer<PDFViewController>();
  int currentPage = 0;
  int pages = 0;
  bool isReady = false;
  String errorMessage = "";
  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height,
      child: Column(
        children: <Widget>[
          Container(
            height: MediaQuery.of(context).size.height - 96,
            child: Stack(
              children: [
                PDFView(
                  filePath: widget.path,
                  enableSwipe: true,
                  swipeHorizontal: true,
                  autoSpacing: false,
                  pageFling: true,
                  pageSnap: true,
                  defaultPage: currentPage,
                  fitPolicy: FitPolicy.WIDTH,
                  preventLinkNavigation: false,
                  onRender: (_pages) {
                    setState(() {
                      pages = _pages;
                      isReady = true;
                    });
                  },
                  onViewCreated: (PDFViewController _controller) {
                    _completerController.complete(_controller);
                  },
                  onPageChanged: (int page, int total) {
                    setState(() {
                      currentPage = page;
                    });
                  },
                  onError: (error) {
                    setState(() {
                      errorMessage = error.toString();
                    });
                  },
                  onPageError: (page, error) {
                    setState(() {
                      errorMessage = "$page: ${error.toString()}";
                    });
                  },
                ),
                errorMessage.isEmpty
                    ? !isReady
                        ? Center(
                            child: CircularProgressIndicator(),
                          )
                        : Container()
                    : Center(
                        child: Text(errorMessage),
                      ),
              ],
            ),
          ),
          Center(child: Text("${1 + currentPage}/$pages"))
        ],
      ),
    );
  }
}
