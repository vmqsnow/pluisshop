import 'package:pluis/models/api_models/coupon.dart';
import 'package:pluis/models/send_models/payment.dart';

class ShoppingCache {
  static final ShoppingCache _singleton = ShoppingCache._internal();

  Coupon coupon;
  String manualCoupon;
  String address;
  String phone;
  String email;
  PaymentType payment;

  factory ShoppingCache() {
    return _singleton;
  }

  ShoppingCache._internal();
}
